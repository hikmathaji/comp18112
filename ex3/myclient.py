"""

IRC client exemplar.

"""

import sys
from ex3utils import Client

import time


class IRCClient(Client):

  def onMessage(self, socket, message):
    # *** process incoming messages here ***
    print message
    return True


# Parse the IP address and port you wish to connect to.
ip = "localhost"# sys.argv[1]
port = 8080 #int(sys.argv[2])
screenName = sys.argv[1]

# Create an IRC client.
client = IRCClient()

# Start server
client.start(ip, port)

# *** register your client here, e.g. ***
client.send('REGISTER %s' % screenName)

while client.isRunning():
  try:
    command = raw_input("> ").strip()
    client.send(command)
  except:
    client.stop();

client.stop()
