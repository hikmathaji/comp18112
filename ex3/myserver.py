import sys
from ex3utils import Server


class MyServer(Server):
  clientCount = 0
  clients = []
  def onStart(self):
    print "Server has started"

  def onStop(self):
    print "Server has stopped"

  def onConnect(self, socket):
      socket.id = self.clientCount
      socket.room = 0 # he is in the lobby now
      self.clients.append(socket);
      self.clientCount = self.clientCount + 1

  def onMessage(self, socket, message):
    (command, sep, parameter) = message.strip().partition(' ')
    #print "A client has just send a message"
    #socket.send("Message is: " + message)
   if command == "REGISTER":
      socket.send("Welcome " + parameter + "!") # welcome message
      socket.screenName = parameter
      print parameter+" has come online"
    elif command == "QUIT":
      self.clients.remove(socket);
      return False;
    else: # command == "MESSAGE"
      print socket.screenName + " just said: " + parameter
      for client in self.clients:
        client.send(socket.screenName + ": " + parameter)
        #print "Socket id: " + str(client.id)
    return True

  def onDisconnect(self, socket):
    self.clientCount = self.clientCount - 1
    if hasattr(socket, "screenName"):
      tempStr = socket.screenName + " has disconnected";
      print tempStr # for server
    else:
      print "A client has disconnected"

ip = "localhost"#sys.argv[1]
port = 8080#int(sys.argv[2])


server = MyServer();
server.start(ip, port)
