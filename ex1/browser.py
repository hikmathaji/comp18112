#!/usr/bin/env python

import urllib

baseUrl = "http://studentnet.cs.manchester.ac.uk/ugt/COMP18112/"
url = 'page3.html'
# For navigating repeatedly
while True:
	data = urllib.urlopen(baseUrl+url)
	tokens = data.read().split()
	nextOneIsHyperlink = False
	# Initializing hyperlinks list and size of it
	numberOfHyperlinks = 0
	hyperlinks = {}
	for token in tokens:
		if token == '<html>' or token == '</html>' or token == '<head>' or token == '</head>' or token == '<body>'  or token == '</body>' or token == '</a>':
			continue
		elif nextOneIsHyperlink:
			url = token[6:-2]
			hyperlinks[numberOfHyperlinks] = url
			numberOfHyperlinks += 1
			nextOneIsHyperlink = False;
		elif token == '<title>':
			print('Page title: '),
		elif token == '</title>':
			print '\n'
		elif token == '<p>':
			print ('PARAGRAPH: '),
		elif token == '</p>':
			print ''
		elif token == '<h1>':
			print ('HEADING: '),
		elif token == '</h1>':
			print ''
		elif token == '<em>':
			print ('\033[1m'),
		elif token == '</em>':
			print ('\033[0;0m')
		elif token == '<a':
			nextOneIsHyperlink = True
		else:
			print (token),

	# Exit if there is no hyperlink to go
	if numberOfHyperlinks == 0:
		break;

	# Iterating hyperlinks and printing in certain format
	for i in range(0, numberOfHyperlinks):
		print (i+1),
		print ' : ', 
		print hyperlinks[i]


	# Reading user choice and parsing it into integer	
	# In case of wrong input, user tries again
	choice = -1	
	# If choice is invalid number, request new number
	while choice < 0 or choice >= numberOfHyperlinks:
		choice = raw_input("Please choose a link: ")
		choice = int(choice) -1

	# Assigning chosen hyperlink to url 
	url = hyperlinks[choice]


