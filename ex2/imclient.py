import im
import json
import datetime
import time
server= im.IMServerProxy("http://webdev.cs.manchester.ac.uk/~mbax2hh2/IMServer.php")

# ChatRoom class

class ChatRoom:
	@staticmethod
	def getChatRoom(index):
		return 	json.loads(server["chatRoom" + str(index)])

	@staticmethod
	def createNewChatRoom(name, username):
		tempChatRoom = ChatRoom()
		tempChatRoom.name = name
		tempChatRoom.createdDate = str(datetime.datetime.now())
		tempChatRoom.createdBy = username
		# setting chatRoomCount
		chatRoomCount = int(server["chatRoomCount"])
		chatRoomCount += 1
		server["chatRoomCount"] = str(chatRoomCount)
		tempChatRoom.text = ""
		tempChatRoom.seenText = "1"
		tempChatRoom.numberOfPeople = 0
		tempChatRoom.index = chatRoomCount
		server["chatRoom" + str(chatRoomCount)] = json.dumps(tempChatRoom, cls=MyEncoder)
		return chatRoomCount

	@staticmethod
	def increaseNumberOfPeople(chatRoom):
		chatRoom['numberOfPeople'] = str(int(chatRoom['numberOfPeople']) + 1)
		ChatRoom.update(chatRoom)
	
	@staticmethod
	def update(chatRoom):
		server["chatRoom" + str(chatRoom['index'])] = json.dumps(chatRoom, cls=MyEncoder)

	@staticmethod
	def changeTypingPerson(chatRoom):
		if chatRoom["typingPerson"] == chatRoom["firstPerson"]:
			chatRoom['typingPerson'] = chatRoom['secondPerson']
		else:
			chatRoom['typingPerson'] = chatRoom['firstPerson']
		ChatRoom.update(chatRoom)


class User:
	@staticmethod
	def getUser(username):
		return json.loads(server[username])

	def getLastSeen(self):
		return self.lastSeen

	@staticmethod
	def createNewUser(username):
		tempUser = User()
		tempUser.username = username
		tempUser.lastSeen = str(datetime.datetime.now())
		server[username] = json.dumps(tempUser, cls=MyEncoder)


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if not isinstance(obj, User) and not isinstance(obj, ChatRoom):
            return super(MyEncoder, self).default(obj)
        return obj.__dict__




print "Welcome to our chat system!"

# Getting username
while True:
	username = raw_input("Please choose a username: ")
	if server[username] != '':
		print "Username already exists, please pick a different one."
	else:
		User.createNewUser(username)
		break

user = User.getUser(username)

server[username] = ''

print "Hello " + username + "."
print "Please enter a chatroom or create your own to chat."

# Getting user choice 
choice = 0
while True:
	print "\n1. Enter existing chat room."
	print "2. Create a new chat room."
	choice = raw_input("\nPlease choose an option: ")
	try:
		choice = int(choice)
		if choice == 1 or choice == 2:
			break
		else: 
			print "Wrong input, please try again..."
	except:
		print "Wrong input, please try again..."

# If user chose to enter a chat room


if choice == 1:
	chatRoomCount = int(server["chatRoomCount"])
	chatRoomName = "chatRoom"
	chatRoomNumber = 1


	# Listing chat rooms
	print "#  Name      Created By       	    CreatedDate"
	while chatRoomNumber <= chatRoomCount:
		chatRoom = ChatRoom.getChatRoom(chatRoomNumber)
		print str(chatRoomNumber) + ". " + chatRoom['name'] + "\t" + chatRoom['createdBy'] + "\t\t" + chatRoom['createdDate']
		chatRoomNumber += 1
	while True:
		if chatRoomCount == 0:
			print "There is no chat room, creating a new chat room."
			choice = 2
			break
		try:
			roomChoice = raw_input("\nPlease choose a chat room.")
			roomChoice = int(roomChoice)
			if roomChoice <= chatRoomCount and roomChoice > 0:
				break
			else: 
				print "Wrong input, please try again..."
		except:
			print "Wrong input, please try again..."


if choice == 2:
	chatRoomName = raw_input("\nPlease enter a name for the chat room: ")
	print "Creating a chat room named " + chatRoomName;
	tempID = ChatRoom.createNewChatRoom(chatRoomName, username)
	chatRoom = ChatRoom.getChatRoom(tempID)



ChatRoom.increaseNumberOfPeople(chatRoom)

if chatRoom['numberOfPeople'] == '1':
	chatRoom["firstPerson"] = username
	chatRoom["typingPerson"] = chatRoom["firstPerson"];
else:
	chatRoom["secondPerson"] = username
ChatRoom.update(chatRoom)
# Waiting someone else to connect

print "Wating someone else to connect."
while chatRoom['numberOfPeople'] != '2':
	chatRoom = ChatRoom.getChatRoom(chatRoom['index'])
	time.sleep(1)
print chatRoom['secondPerson'] + " connected"
# Let the chat begin!!!



# Chatting forever and ever...
while True:
	chatRoom = ChatRoom.getChatRoom(chatRoom['index'])
	#print chatRoom["typingPerson"] + " is typing"
	if chatRoom['seenText'] == '0':
		print chatRoom['text']
		chatRoom['seenText'] = '1'
		ChatRoom.update(chatRoom)
	elif chatRoom["typingPerson"] == username:
		text = raw_input(username + ": ")
		chatRoom["text"] = username + ": " +text
		chatRoom["seenText"] = '0'
		ChatRoom.changeTypingPerson(chatRoom)
		ChatRoom.update(chatRoom)
		time.sleep(1)
	